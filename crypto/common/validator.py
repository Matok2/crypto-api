def length(string, minimum, maximum = None):
    if maximum is None:
        maximum = minimum
        
    return minimum <= len(string) <= maximum
    
    
def fixed_length(size):
    def validate(value):
        if length(value, size):
            return value                        
        raise ValueError("Expected length <{}>, current length <{}>".format(size, len(value)))
    
    return validate
    
    
def between_length(minimum, maximum):
    def validate(value):
        if length(value, minimum, maximum):
            return value                        
        raise ValueError("Expected length <{},{}>, current length <{}>".format(minimum, maximum, len(value)))
    
    return validate


def positive_float(value):
    if not isinstance(value, float):
        raise ValueError("{} is not float!".format(value))

    if value <= 0:
        raise ValueError("{:f} must be positive!".format(value))

    return value
