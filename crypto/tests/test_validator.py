import unittest
from crypto.common.validator import length
from crypto.common.validator import positive_float


class TestLengthValidator(unittest.TestCase):
    def test_min_length(self):
        self.assertTrue(length('abc', 3))

    def test_max_length(self):
        self.assertTrue(length('abcde', 3, 5))

    def test_min_length_fail(self):
        self.assertFalse(length('abc', 4))
        
    def test_max_length_fail(self):
        self.assertFalse(length('abcedf', 1, 5))

    def test_positive_float_but_string(self):
        with self.assertRaises(ValueError):
            positive_float("ten")

    def test_positive_float_but_negative(self):
        with self.assertRaises(ValueError):
            positive_float(-56.9)

    def test_positive_float_valid(self):
        self.assertEquals(1.2, positive_float(1.2))

if __name__ == '__main__':
    unittest.main()
