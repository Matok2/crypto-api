from flask_restful import Resource
from flask_restful import abort, reqparse, marshal_with
from db import session
from crypto.entities.exchange import Exchange as ExchangeEntity
from crypto.entities.currency import Currency as CurrencyEntity
from crypto.common.validator import fixed_length, between_length
from crypto.resources.exchange_fields import exchange_fields


class Exchange(Resource):
    @marshal_with(exchange_fields)
    def post(self):
        args = self._post_arguments()
        exchange = self._get_exchange_by_name(args['name'])
        if exchange is not None:
            abort(409, message="Exchange office with the same name already exists id={}!".format(exchange.id))

        currency = self._get_currency_by_shortcut(args['shortcut'])
        if currency is None:
            abort(400, message="There is no {} currency, please create this currency first".format(args['shortcut']))

        exchange = ExchangeEntity(name=args['name'], currency_id=currency.id)
        session.add(exchange)
        session.commit()
        
        return exchange

    def _post_arguments(self):
        parser = reqparse.RequestParser()
        parser.add_argument("name", type=between_length(3, 30), required=True)
        parser.add_argument("cryptoCurrency", type=fixed_length(3), required=True, dest="shortcut")
        
        return parser.parse_args()

    def _get_exchange_by_name(self, name):
        return session.query(ExchangeEntity).filter_by(name=name).one_or_none()

    def _get_currency_by_shortcut(self, shortcut):
        return session.query(CurrencyEntity).filter_by(shortcut=shortcut).one_or_none()
