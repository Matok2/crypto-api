from flask_restful import Resource
from flask_restful import abort, reqparse
from db import session
from crypto.entities.exchange import Exchange
from crypto.entities.currency import Currency
from crypto.entities.exchange_rate import ExchangeRate

    
class CryptoCurrency(Resource):
    def put(self, exchange_id):
        exchange = self._get_exchange_or_fail(exchange_id)
        exchange_rates = self._put_arguments().exchange
        
        for exchange_rate in exchange_rates:
            exchange_rate = ExchangeRate(rate=exchange_rate["rate"], exchange_id=exchange.id, currency_id=exchange_rate["currency_id"])
            session.add(exchange_rate)
            
        session.commit()
        
        return None, 201
    
    def _get_exchange_or_fail(self, id):
        exchange = session.query(Exchange).filter_by(id=id).one_or_none()
        if exchange is None:
            abort(404)
            
        return exchange
        
    def _get_currency(self, shortcut):
        return session.query(Currency).filter_by(shortcut=shortcut).first() 

    def _put_arguments(self):
        parser = reqparse.RequestParser()
        parser.add_argument("exchange", required=True, type=self._exchange_list, action='append')
                
        return parser.parse_args()   

    def _exchange_list(self, item):
        if "currency" not in item:
            raise ValueError("Missing key `currency`")
          
        if "rate" not in item:
            raise ValueError("Missing key `rate`")
          
        item["rate"] = float(item["rate"])
        if item["rate"] <= 0.0:
            raise ValueError("Invalid value for `rate`: {:f}".format(item["rate"]))
        
        currency = self._get_currency(item["currency"])
        
        if currency is None:
            raise ValueError("There is no `currency` {}".format(item["currency"]))
        
        item["currency_id"] = currency.id
        
        return item
