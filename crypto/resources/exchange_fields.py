from flask_restful import fields


exchange_fields = {
    "id": fields.Integer,
    "name": fields.String,
    "currency": {
        "name": fields.String(attribute=lambda exchange: exchange.currency.name),
        "shortcut": fields.String(attribute=lambda exchange: exchange.currency.shortcut),
    },
    "balance": fields.String(attribute=lambda c: "{:f}".format(c.balance)),
}
