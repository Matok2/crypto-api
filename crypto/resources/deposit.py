from flask_restful import Resource
from flask_restful import abort, reqparse, marshal_with
from db import session
from crypto.entities.exchange import Exchange as ExchangeEntity
from crypto.common.validator import positive_float
from crypto.resources.exchange_fields import exchange_fields


class Deposit(Resource):
    @marshal_with(exchange_fields)
    def post(self, id):
        exchange = self._get_exchange_or_fail(id)
        args = self._post_arguments()
        exchange.balance = ExchangeEntity.balance + args.amount
        session.commit()
        
        return exchange, 201
        
    def _get_exchange_or_fail(self, id):
        exchange = session.query(ExchangeEntity).filter_by(id=id).one_or_none()
        if exchange is None:
            abort(404)
            
        return exchange

    def _post_arguments(self):
        parser = reqparse.RequestParser()
        parser.add_argument("amount", type=positive_float, required=True)
        
        return parser.parse_args()
