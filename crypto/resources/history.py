from flask_restful import Resource
from flask_restful import reqparse, fields, marshal
from db import session
from crypto.entities.trade import Trade
from crypto.entities.exchange import Exchange
from crypto.entities.exchange_rate import ExchangeRate
from crypto.entities.currency import Currency
from sqlalchemy.orm import aliased
from datetime import datetime

trade_fields = {
    "id": fields.Integer(attribute="trade.id"),
    "exchange": fields.String(attribute="rate.exchange.name"),
    "exchange_rate": fields.Float(attribute=lambda x: "{:f}".format(x['rate'].rate)),
    "input": {
        "amount": fields.Float(attribute="trade.input_amount"),
        "currency": fields.String(attribute="rate.exchange.currency.shortcut"),
    },
    "output": {
        "amount": fields.Float(attribute="trade.output_amount"),
        "currency": fields.String(attribute="rate.currency.shortcut"),
    },
    "date": fields.DateTime(attribute="trade.traded_at")
}


class History(Resource):
    def get(self):
        args = self._get_arguments()
        in_currency = aliased(Currency)
        out_currency = aliased(Currency)

        query = session.query(Trade, ExchangeRate).\
            join(ExchangeRate).\
            join(Exchange, Exchange.id == ExchangeRate.exchange_id).\
            join(in_currency, in_currency.id == Exchange.currency_id).\
            join(out_currency, out_currency.id == ExchangeRate.currency_id)

        if args.exchange_id is not None:
            query = query.filter(ExchangeRate.exchange_id == args.exchange_id)

        if args.in_currency is not None:
            query = query.filter(in_currency.shortcut == args.in_currency)

        if args.out_currency is not None:
            query = query.filter(out_currency.shortcut == args.out_currency)

        if args.date_from is not None:
            query = query.filter(Trade.traded_at >= args.date_from)

        if args.date_to is not None:
            query = query.filter(Trade.traded_at >= args.date_to)

        query = query.limit(args.limit)
        query = query.offset(args.offset)

        trades = []
        for trade, exchange_rate in query:
            trades.append(marshal({'trade': trade, 'rate': exchange_rate}, trade_fields))

        return trades

    def _get_arguments(self):
        parser = reqparse.RequestParser()
        parser.add_argument('offset', type=int, location='args')
        parser.add_argument('limit', type=int, location='args')
        parser.add_argument('exchange_id', type=int, location='args')
        parser.add_argument('in_currency', type=str, location='args')
        parser.add_argument('out_currency', type=str, location='args')
        parser.add_argument('date_from', type=lambda x: datetime.strptime(x, '%Y-%m-%dT%H:%M:%S'))
        parser.add_argument('date_to', type=lambda x: datetime.strptime(x, '%Y-%m-%dT%H:%M:%S'))

        return parser.parse_args()
