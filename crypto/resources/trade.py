from flask_restful import Resource
from flask_restful import abort, reqparse
from sqlalchemy import desc
from db import session
from crypto.common.validator import fixed_length
from crypto.entities.currency import Currency
from crypto.entities.exchange import Exchange
from crypto.entities.exchange_rate import ExchangeRate
from crypto.entities.trade import Trade as TradeEntity
import decimal


class Trade(Resource):
    def post(self, exchange_id):
        args = self._post_arguments()
        exchange = self._get_exchange(exchange_id)
        currency = self._get_currency(args.currency)
        
        if exchange is None:
            abort(404, message="Exchange <ID:{}> not found!".format(exchange_id))
                                
        if currency is None:
            abort(404, message="Currency <{}> not found!".format(args.currency))

        exchange_rate = self._get_exchage_rate(exchange_id, currency.id)
        if exchange_rate is None:
            abort(404, message="Exchange rate not found for <{}>! Cannot do trade.".format(currency.shortcut))
        
        if exchange.balance < args.amount:
            abort(400, message="insufficient balance of <{}> in exchange <ID:{}>".format(exchange.currency.shortcut, exchange_id))
        
        conversion = exchange_rate.rate*decimal.Decimal(args.amount)
        exchange.balance = Exchange.balance - args.amount
        trade = TradeEntity(input_amount=args.amount, output_amount=conversion, exchange_rate_id=exchange_rate.id)
        session.add(trade)
        
        session.commit()
        
        return None, 201

    def _get_exchange(self, id):
        return session.query(Exchange).filter_by(id=id).first()

    def _get_currency(self, shortcut):
        return session.query(Currency).filter_by(shortcut=shortcut).first()

    def _get_exchage_rate(self, exchange_id, currency_id):
        return session.query(ExchangeRate).\
               filter_by(exchange_id=exchange_id, currency_id=currency_id).\
               order_by(desc(ExchangeRate.created_at)).\
               first()
                        
    def _post_arguments(self):
        parser = reqparse.RequestParser()
        parser.add_argument("amount", type=float, required=True)
        parser.add_argument("currency", type=fixed_length(3), required=True)
        
        return parser.parse_args()
