from sqlalchemy import Column, ForeignKey, Integer, Numeric, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
from db import Base


class ExchangeRate(Base):
    __tablename__ = 'exchange_rate'

    id = Column(Integer(), primary_key=True)
    rate = Column(Numeric(asdecimal=True, precision=20, scale=8), nullable=False)
    exchange_id = Column(Integer, ForeignKey("exchange.id"), nullable=False)
    currency_id = Column(Integer, ForeignKey("currency.id"), nullable=False)
    created_at = Column(DateTime, server_default=func.now())
     
    exchange = relationship("Exchange")
    currency = relationship("Currency")

    def __repr__(self):
        return "<ER (id='{}', rate='{}')>".format(self.id, self.rate)
