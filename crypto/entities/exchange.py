from sqlalchemy import Column, ForeignKey, Integer, String, Numeric
from sqlalchemy.orm import relationship
from db import Base


class Exchange(Base):
    __tablename__ = 'exchange'

    id = Column(Integer(), primary_key=True)
    currency_id = Column(Integer, ForeignKey("currency.id"), nullable=False)
    name = Column(String(30), nullable=False)
    balance = Column(Numeric(asdecimal=True, precision=20, scale=8), nullable=False, default="0.0", server_default="0.0")

    currency = relationship("Currency")

    def __repr__(self):
        return "<Exchange (name='{}', balance={:f})>".format(self.name, self.balance)
