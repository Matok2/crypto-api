from sqlalchemy import Column, ForeignKey, Integer,  Numeric, String, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
from db import Base


class Trade(Base):
    __tablename__ = 'trade'

    id = Column(Integer, primary_key=True)
    exchange_rate_id = Column(Integer(), ForeignKey("exchange_rate.id"), nullable=False)
    input_amount = Column(Numeric(asdecimal=True, precision=20, scale=8), nullable=False)
    output_amount = Column(Numeric(asdecimal=True, precision=20, scale=2), nullable=False)
    traded_at = Column(DateTime, server_default=func.now())

    exchange_rate = relationship("ExchangeRate")

    def __repr__(self):
        return "<Trade(in='{}')>".format(self.input_amount)
