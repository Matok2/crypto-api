from sqlalchemy import Column, Integer, String
from db import Base


class Currency(Base):
    __tablename__ = 'currency'

    id = Column(Integer(), primary_key=True)
    name = Column(String(30), nullable=False)
    shortcut = Column(String(3), nullable=False, unique=True)

    def __repr__(self):
        return "<Currency ([{}] name='{}', shortcut='{}')>".format(self.id, self.name, self.shortcut)
