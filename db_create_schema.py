import argparse
from crypto.entities.currency import Currency
from crypto.entities.exchange import Exchange
from crypto.entities.exchange_rate import ExchangeRate
from crypto.entities.trade import Trade
from db import Base, Engine, session
from sqlalchemy_utils import create_database

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--connection", help="Print database connection DSN", action="store_true")
parser.add_argument("-d", "--drop", help="Drop DB scheme before creation", action="store_true")
parser.add_argument("-D", "--database", help="Create database with name in your configuration", action="store_true")

args = parser.parse_args()

if args.connection:
  print(Engine.url)
  exit(0)

if args.drop:
  print(" ** Droping schema...")
  Base.metadata.drop_all(Engine)

if args.database:
  print(" ** Creating DB...")
  create_database(Engine.url)
  
print(" ** Creating schema...")  
Base.metadata.create_all(Engine)

print(" ** Creating currency records...")
usd = Currency(name="American Dollar", shortcut="USD")
eur = Currency(name="Euro", shortcut="EUR")
cad = Currency(name="Canadian Dollar", shortcut="CAD")
yen = Currency(name="Japanese Yen", shortcut="JPY")
session.add(usd)
session.add(eur)
session.add(cad)
session.add(yen)

btc = Currency(name="Bitcoin", shortcut="BTC")
bch = Currency(name="Bitcoin Cash", shortcut="BCH")
ltc = Currency(name="Litecoin", shortcut="LTC")
xmr = Currency(name="Monero", shortcut="XMR")
session.add(btc)
session.add(bch)
session.add(ltc)
session.add(xmr)

session.commit()

print(" ** OK!")