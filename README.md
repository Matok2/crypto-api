# Crypto API

Python REST API based on:

[FlaskRESTful](https://flask-restful.readthedocs.io/en/latest/)

[SQLAlchemy](https://www.sqlalchemy.org/)

## Installation
```
pip install -r requirements.txt 
```

## Database

Database connection string is in file `db.py` and it's pre-set to `mysql://root:root@localhost/crypto`

Database schema could be created by
```
python db_create_schema.py
```

![DB table](./docs/db.png)

**Usage:**
```
usage: db_create_schema.py [-h] [-c] [-d] [-D]

optional arguments:
  -h, --help        show this help message and exit
  -c, --connection  Print database connection DSN
  -d, --drop        Drop DB scheme before creation
  -D, --database    Create database with name in your configuration
```

**(Crypto) Currencies:**

You can use any of this currencies:

| shortcut | name |
|---|---|
|USD|American Dollar|
|EUR|Euro|
|CAD|Canadian Dollar|
|JPY|Japanese Yen|
|BTC|Bitcoin|
|BCH|Bitcoin Cash|
|LTC|Litecoin|
|XMR|Monero|

Note: this app doesn't distinguish between "regular" currency and "crypto" currency.

## Application - API

### Server

Run API server:
```
python api.py
```

### Client

**A) Create exchange (exchange office):**
```
curl http://127.0.0.1:5000/api/v1/crypto/exchanges -X POST --data '{"name": "Iron Bank", "cryptoCurrency": "BTC"}' -H "Content-Type: application/json"
```
```json
{
    "id": 1,
    "name": "Iron Bank",
    "currency": {
        "name": "Bitcoin",
        "shortcut": "BTC"
    },
    "balance": "0.00000000"
}
```

**B) Deposit crypto currency:**
```
curl http://127.0.0.1:5000/api/v1/crypto/exchanges/1 -X POST --data '{"amount": 2.54}' -H "Content-Type: application/json"
```

```json
{
    "id": 1,
    "name": "Iron Bank",
    "currency": {
        "name": "Bitcoin",
        "shortcut": "BTC"
    },
    "balance": "2.54000000"
}
```

If call same endpoint again the result will be:
```json
{
    "id": 1,
    "name": "Bitcoin",
    "currency":  {
        "name": "Bitcoin",
        "shortcut": "BTC"
    },
    "balance": "5.08000000"
}
```

**C) Create/Update exchange rates** 
```
curl http://127.0.0.1:5000/api/v1/crypto/exchanges/1/currencies -X PUT --data '{"exchange": [{"currency": "USD", "rate": 9947.4}, {"currency": "CAD", "rate": 13183.7}]}' -H "Content-Type: application/json"
```
This create two records:

BTC/USD with exchange rate 1 BTC = 9947.4 USD

BTC/USD with exchange rate 1 BTC = 13183.7 CAD

These rates are marked by actual date and during trade (next endpoint) most actual exchanges rate is used.

Successful response is **201** (created) without body.

**D) Trade (sell crypto currency and receive "regular" currency)** 
```
curl http://127.0.0.1:5000/api/v1/crypto/exchanges/1/trades -X POST --data '{"amount": 0.5, "currency": "USD"}' -H "Content-Type: application/json"
```
Request sell 0.5 BTC (Exchange [ID: 1] works with BTC) and appropriate amount of USD currency will be stored in trade transaction.

**E) Show trade history**
```
curl "http://127.0.0.1:5000/api/v1/crypto/history"
```

```json
[
    {
        "id": 1,
        "exchange": "Iron Bank",
        "exchange_rate": 9947.4,
        "input": {
            "amount": 0.3,
            "currency": "BTC"
        },
        "output": {
            "amount": 2984.22,
            "currency": "USD"
        },
        "date": "Sun, 16 Feb 2020 21:43:26 -0000"
    },
    {
        "id": 3,
        "exchange": "Iron Bank",
        "exchange_rate": 9947.4,
        "input": {
            "amount": 0.95,
            "currency": "BTC"
        },
        "output": {
            "amount": 9450.03,
            "currency": "USD"
        },
        "date": "Sun, 16 Feb 2020 23:08:40 -0000"
    }
]
```

More examples:

```
# show only 2 records
curl "http://127.0.0.1:5000/api/v1/crypto/history?limit=2"

# show only 2 records and skip first two
curl "http://127.0.0.1:5000/api/v1/crypto/history?limit=2&offset=2"

# filter by in/out currency
curl "http://127.0.0.1:5000/api/v1/crypto/history?in_currency=BTC&out_currency=CAD"

# filter by dates
curl "http://127.0.0.1:5000/api/v1/crypto/history?date_from=2020-01-01T23:13:30&date_to=2020-04-01T23:13:30"
```

## Tests

Only basic unit-test are provided, you could run them:
```
python -m unittest
```
