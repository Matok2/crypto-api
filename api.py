from flask import Flask
from flask_restful import Api
from crypto.resources.deposit import Deposit
from crypto.resources.exchange import Exchange
from crypto.resources.cryptocurrency import CryptoCurrency
from crypto.resources.trade import Trade
from crypto.resources.history import History

app = Flask(__name__)
api = Api(app, '/api/v1/crypto')

api.add_resource(Exchange, '/exchanges')
api.add_resource(Deposit, '/exchanges/<int:id>')
api.add_resource(CryptoCurrency, '/exchanges/<int:exchange_id>/currencies')
api.add_resource(Trade, '/exchanges/<int:exchange_id>/trades')
api.add_resource(History, '/history')

if __name__ == '__main__':
    app.run(debug=True)
