from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

connection_dsn = "mysql://root:root@localhost/crypto"

Base = declarative_base()
Engine = create_engine(connection_dsn, echo=True)
Session = sessionmaker(bind=Engine)
session = Session()
